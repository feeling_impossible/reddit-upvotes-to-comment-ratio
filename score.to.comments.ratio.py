

##
# This saves all the stats to a stats-type-period.json file.
# If you run it a second time, it will generate the stats from the saved file rather than download everything again.
# If you want it to run everything from scratch, delete the stats file


##
# make sure these subeddits are analyzed
testSubs = ['The_Donald', 'politics']

# Political subreddits with more than 100,000 subscribers
# Generated from: https://www.reddit.com/r/politics/wiki/relatedsubs - See political.subs.py
politicalSubs = ['Anarchism', 'Bad_Cop_No_Donut', 'business', 'Conservative', 'conspiracy', 'Economics', 'environment', 'geopolitics', 'history', 'IWantOut', 'Libertarian', 'MensRights', 'NeutralPolitics', 'news', 'PoliticalDiscussion', 'PoliticalHumor', 'politics', 'privacy', 'SandersForPresident', 'socialism', 'The_Donald', 'ukpolitics', 'WikiLeaks', 'worldnews', 'worldpolitics']

##
# Analyzes the top threads from each subreddit
# week: top posts of the last week
# month: top posts of the last month
# year: top posts of the last year
# all: top post of all time in the sub
period = 'year'





import json
import requests
import os.path
from pprint import pprint
from time import sleep
from time import gmtime
from calendar import timegm
import matplotlib.pyplot as plt


def getThreads(subreddit, after = None):
	url = 'https://www.reddit.com/r/' + subreddit + '/top.json?sort=top&t=' + period + '&limit=100'
	if (after):
		url += '&after=t3_' + after
		
	# pretend we are a browser. shhhhhh... I'm hunting wabbits
	headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'}
	response = requests.get(url, headers=headers)
	links = json.loads(response.text)['data']['children']

	threads = []
	for data in links:
		thread = {}
		
		# API labels to save
		labels = ['id', 'subreddit', 'score', 'title', 'created_utc', 'num_comments'];
		failed = False
		for label in labels:
			if label not in data['data']:
				failed = True
				continue
				
			thread[label] = data['data'][label]
			
		if failed:
			continue
			
		# ignore threads younger than 24 hours old
		if (timegm(gmtime()) - thread['created_utc']) > (60 * 60 * 24):
			threads.append(thread)
	
	print('found: {:4d} - {}'.format(len(links), url))
	if (len(links)):
		# play nice with Reddit's server
		sleep(1)
		threads += getThreads(subreddit, links[-1]['data']['id'])

	return threads

##
# get stats
def getStats(period, political):
	statsFile = 'stats-' + period + '-'
	if political:
		statsFile += 'political'
	else:
		statsFile += 'all'
	statsFile += '.json'

	stats = {}
	if os.path.isfile(statsFile):
		##
		# load stats from file
		with open(statsFile) as file:
			stats = json.load(file)
		
	else:
		##
		# generate stats. this is going to take a while
		subreddits = []
		
		##
		# get list of subreddits to analyze
		if political:
			for sub in politicalSubs:
				if sub not in subreddits:
					subreddits.append(sub)
					
		else:
			##
			# Get a list of subreddits to test from /r/all
			threads = getThreads('all')
			print ('total: {:4d} - threads older than 24 hours\n\n'.format(len(threads)))
			
			for thread in threads:
				if thread['subreddit'] not in subreddits:
					subreddits.append(thread['subreddit']);


		##
		# make sure our testSubs are in our list of subreddits to analyze
		for sub in testSubs:
			if sub not in subreddits:
				subreddits.append(sub)

		##
		# sort subreddits
		subreddits.sort(key=lambda s: s.lower())

		##
		# display list of subreddits that will be analyzed
		print ('Analyzing Subreddits: ', end = '')
		for sub in subreddits:
			print (sub + ', ', end = '')
		print ('\n\n')
		
		##
		# analyze each subreddit
		for sub in subreddits:
			print('subreddit:', sub)
			threads = getThreads(sub)
			print ('total: {:4d} - threads older than 24 hours\n\n'.format(len(threads)))
			
			if sub not in stats:
				stats[sub] = {
					'comments': 0,
					'score': 0
				}
			
			for thread in threads:
				stats[sub]['comments'] += thread['num_comments']
				stats[sub]['score'] += thread['score']
				
			if stats[sub]['comments'] > 0:
				stats[sub]['ratio'] = stats[sub]['score'] / stats[sub]['comments']
			else:
				stats[sub]['ratio'] = 0

		##
		# save stats file
		file = open(statsFile, 'w') 
		file.write(json.dumps(stats, indent = 4)) 
		file.close()

	return stats


def graph(stats, type, political):
	##
	# sort stats by type (ratio, comments, score)
	stats = {k: stats[k] for k in sorted(stats, key = lambda x: stats[x][type])}

	##
	# create a bar graph of results
	labels = []
	values = []
	for sub in stats:
		labels.append(sub)
		values.append(stats[sub][type])
		
	plt.figure(figsize=(5, 20))
	plt.subplots_adjust(left = .35, bottom = .05, top = .95)
	plt.barh(labels, values)
	plt.ylim(-1, len(labels))
	plt.xticks(rotation=90)
	plt.gca().get_xaxis().get_major_formatter().set_useOffset(False)
	
	label = '';
	if type == 'ratio':
		label = 'Upvote to Comment Ratio'
	elif type == 'comments':
		label = 'Number of Comments'
	elif type == 'score':
		label = 'Number of Upvotes'
	
	if political:
		title = 'Political'
	else:
		title = 'All'
	title += ' Subreddits\n' + label + '\nTop: ' + str(period)
	plt.title(title)
	

	for sub in testSubs:
		plt.gca().get_yticklabels()[labels.index(sub)].set_color("red")

	plotName = 'SubStats-' + str(period) + '-'
	if political:
		plotName += 'political'
	else:
		plotName += 'all'
	plotName += '-' + type + '.png'

	print('saving graph:', plotName)
	plt.savefig(plotName)
	plt.close()


##
# use this to generate every type of graph
# for political in [True, False]:
	# for period in ['week', 'month', 'year', 'all']:
		# stats = getStats(period, political)
		
		# for type in ['ratio', 'comments', 'score']:
			# graph(stats, type, political)
			
			
for political in [True, False]:
	stats = getStats(period, political)
	
	for type in ['ratio', 'comments', 'score']:
		graph(stats, type, political)






















